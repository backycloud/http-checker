#!/usr/bin/python
# -*- coding: utf-8 -*-

import requests
import time
import logging
import smtplib
import getopt
import sys
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders

logging.basicConfig(format = u'%(levelname)-8s [%(asctime)s] %(message)s', level = logging.INFO, filename = ('http_checker.log'))

fromaddr = "sometestuserme@gmail.com"
mailpassword = "yRKO0MFlg4K7"

count = 0

if len(sys.argv) < 4:
   print "Example: http-checker -h https://example.com -m admin@mail.com, or python http-checker -h https://example.com -m admin@mail.com"
   sys.exit()
print 'ARGV      :', sys.argv[1:]

options, remainder = getopt.gnu_getopt(sys.argv[1:], 'h:m', ('host=', 'mail=',))
domain = sys.argv[2]
toaddr = sys.argv[4]

def sendmail():
   msg = MIMEMultipart()
   msg['From'] = fromaddr
   msg['To'] = toaddr
   msg['Subject'] = "Error! http_checker."
   body = "Admin something going wrong, check log!!!"
   msg.attach(MIMEText(body, 'plain'))
   attachment = open("http_checker.log", "rb")
   p = MIMEBase('application', 'octet-stream')
   p.set_payload((attachment).read())
   encoders.encode_base64(p)
   p.add_header('Content-Disposition', "attachment; filename=http_checker.log")
   msg.attach(p)
   s = smtplib.SMTP('smtp.gmail.com', 587)
   s.starttls()
   s.login(fromaddr, mailpassword)
   text = msg.as_string()
   s.sendmail(fromaddr, toaddr, text)
   s.quit()

while True:
    try:
        r = requests.head('{0}'.format(domain))
        logging.info('{0} {1}'.format(domain, r.status_code))
        time.sleep(30)
        continue
    except requests.ConnectionError:
        time.sleep(30)
        count = count + 1
        if count == 1:
            logging.info('{0} '.format(domain) + 'error')
            sendmail()
        else:
            continue
        continue
